#include "engine/application/Callback.hpp"

namespace green
{
    BgfxCallback::~BgfxCallback()
    {
    }

    void BgfxCallback::fatal(bgfx::Fatal::Enum _code, const char* _str)
    {
        // Something unexpected happened, inform user and bail out.
        bx::debugPrintf("Fatal error: 0x%08x: %s", _code, _str);

        // Must terminate, continuing will cause crash anyway.
        abort();
    }

    void BgfxCallback::traceVargs(const char* _filePath, uint16_t _line, const char* _format, va_list _argList) 
    {
        bx::debugPrintf("%s (%d): ", _filePath, _line);
        bx::debugPrintfVargs(_format, _argList);
    }

    uint32_t BgfxCallback::cacheReadSize(uint64_t _id)
    {
        char filePath[256];
        bx::snprintf(filePath, sizeof(filePath), "temp/%016" PRIx64, _id);

        // Use cache id as filename.
        bx::FileReaderI* reader = entry::getFileReader();
        bx::Error err;
        if (bx::open(reader, filePath, &err))
        {
            uint32_t size = (uint32_t)bx::getSize(reader);
            bx::close(reader);
            // Return size of shader file.
            return size;
        }

        // Return 0 if shader is not found.
        return 0;
    }

    bool BgfxCallback::cacheRead(uint64_t _id, void* _data, uint32_t _size)
    {
        char filePath[256];
        bx::snprintf(filePath, sizeof(filePath), "temp/%016" PRIx64, _id);

        // Use cache id as filename.
        bx::FileReaderI* reader = entry::getFileReader();
        bx::Error err;
        if (bx::open(reader, filePath, &err))
        {
            // Read shader.
            uint32_t result = bx::read(reader, _data, _size, &err);
            bx::close(reader);

            // Make sure that read size matches requested size.
            return result == _size;
        }

        // Shader is not found in cache, needs to be rebuilt.
        return false;
    }

    void BgfxCallback::cacheWrite(uint64_t _id, const void* _data, uint32_t _size)
    {
        char filePath[256];
        bx::snprintf(filePath, sizeof(filePath), "temp/%016" PRIx64, _id);

        // Use cache id as filename.
        bx::FileWriterI* writer = entry::getFileWriter();
        bx::Error err;
        if (bx::open(writer, filePath, false, &err))
        {
            // Write shader to cache location.
            bx::write(writer, _data, _size, &err);
            bx::close(writer);
        }
    }

    void BgfxCallback::screenShot(const char* _filePath, uint32_t _width, uint32_t _height, uint32_t _pitch, const void* _data, uint32_t /*_size*/, bool _yflip)
    {
    }

    void BgfxCallback::captureBegin(uint32_t _width, uint32_t _height, uint32_t /*_pitch*/, bgfx::TextureFormat::Enum /*_format*/, bool _yflip)
    {
    }

    void BgfxCallback::captureEnd()
    {
    }

    void BgfxCallback::captureFrame(const void* _data, uint32_t /*_size*/)
    {
    }
}