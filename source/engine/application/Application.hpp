#pragma once

#include "engine/bgfx_utils/common.h"
#include "engine/bgfx_utils/bgfx_utils.h"

#include "engine/application/Callback.hpp"

#include <memory>

namespace green
{
    class SceneRenderer;

    class Application : public entry::AppI
    {
    public:
                        Application();
                        ~Application();

        virtual void    init(int _argc, char** _argv) BX_OVERRIDE;
        virtual int     shutdown() BX_OVERRIDE;

        bool            update() BX_OVERRIDE;

    private:
        std::unique_ptr<SceneRenderer>  m_sceneRenderer = nullptr;
        
        BgfxCallback                    m_callback;
        
        uint32_t                        m_width;
        uint32_t                        m_height;
        uint32_t                        m_debug;
        uint32_t                        m_reset;
        int64_t                         m_timeOffset;
    };
}
