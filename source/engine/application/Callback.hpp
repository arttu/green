#pragma once

#include "engine/bgfx_utils/common.h"
#include "engine/bgfx_utils/bgfx_utils.h"

#include "bx/allocator.h"
#include "bx/string.h"
#include "bx/crtimpl.h"

#define __STDC_FORMAT_MACROS

#include <compat/msvc/inttypes.h>

namespace green
    {
    struct BgfxCallback : public bgfx::CallbackI
    {
        virtual ~BgfxCallback();

        virtual void fatal(
            bgfx::Fatal::Enum _code, 
            const char* _str) BX_OVERRIDE;

        virtual void traceVargs(
            const char* _filePath, 
            uint16_t _line, 
            const char* _format, 
            va_list _argList) BX_OVERRIDE;

        virtual uint32_t cacheReadSize(
            uint64_t _id) BX_OVERRIDE;

        virtual bool cacheRead(
            uint64_t _id, 
            void* _data, 
            uint32_t _size) BX_OVERRIDE;

        virtual void cacheWrite(
            uint64_t _id, 
            const void* _data, 
            uint32_t _size) BX_OVERRIDE;

        virtual void screenShot(
            const char* _filePath, 
            uint32_t _width, 
            uint32_t _height, 
            uint32_t _pitch, 
            const void* _data, 
            uint32_t /*_size*/, 
            bool _yflip) BX_OVERRIDE;

        virtual void captureBegin(
            uint32_t _width, 
            uint32_t _height, 
            uint32_t /*_pitch*/, 
            bgfx::TextureFormat::Enum /*_format*/,
            bool _yflip) BX_OVERRIDE;

        virtual void captureEnd() BX_OVERRIDE;

        virtual void captureFrame(
            const void* _data, 
            uint32_t /*_size*/) BX_OVERRIDE;
    };
}

