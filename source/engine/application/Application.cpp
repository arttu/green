#include "engine/application/Application.hpp"

#include "engine/system/System.hpp"
#include "engine/renderer/SceneRenderer.hpp"

namespace green
{
	Application::Application()
	{
	}

	Application::~Application()
	{
	}

	void Application::init(int _argc, char** _argv)
	{
		Args args(_argc, _argv);

		m_width = 1280;
		m_height = 720;
		m_debug = BGFX_DEBUG_TEXT;
		m_reset = BGFX_RESET_VSYNC;

		system::init();

		bgfx::init(args.m_type, args.m_pciId, 0, &m_callback);
		bgfx::reset(m_width, m_height, m_reset);

		// Enable debug text.
		bgfx::setDebug(m_debug);

		// Set view 0 clear state.
		bgfx::setViewClear(0
			, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH
			, 0x303030ff
			, 1.0f
			, 0
		);

		m_sceneRenderer = std::make_unique<SceneRenderer>();

		m_timeOffset = bx::getHPCounter();
	}

	int Application::shutdown()
	{
		m_sceneRenderer = nullptr;

		bgfx::shutdown();

		return 0;
	}

	bool Application::update()
	{
		if (!entry::processEvents(m_width, m_height, m_debug, m_reset))
		{
			int64_t now = bx::getHPCounter();
			static int64_t last = now;
			const int64_t frameTime = now - last;
			last = now;
			const double freq = double(bx::getHPFrequency());
			const double toMs = 1000.0 / freq;

			float time = (float)((now - m_timeOffset) / double(bx::getHPFrequency()));

			// Use debug font to print information about this example.
			bgfx::dbgTextClear();
			bgfx::dbgTextPrintf(0, 1, 0x4f, "bgfx/examples/01-cube");
			bgfx::dbgTextPrintf(0, 2, 0x6f, "Description: Rendering simple static mesh.");
			bgfx::dbgTextPrintf(0, 3, 0x0f, "Frame: % 7.3f[ms]", double(frameTime)*toMs);

			bgfx::setViewName(0, "test");

			m_sceneRenderer->update(m_width, m_height, time);

			// Advance to next frame. Rendering thread will be kicked to
			// process submitted rendering primitives.
			bgfx::frame();

			return true;
		}

		return false;
	}

}