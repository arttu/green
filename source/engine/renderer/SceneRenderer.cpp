#include "engine/renderer/SceneRenderer.hpp"

#include "bgfx/embedded_shader.h"

#include "engine/shaders/vs_cubes.bin.h"
#include "engine/shaders/fs_cubes.bin.h"

#include "engine/scene/ComponentStorage.hpp"

namespace green
{
    static const bgfx::EmbeddedShader s_embeddedShaders[] =
    {
        BGFX_EMBEDDED_SHADER(vs_cubes),
        BGFX_EMBEDDED_SHADER(fs_cubes),

        BGFX_EMBEDDED_SHADER_END()
    };

    struct PosColorVertex
    {
        float m_x;
        float m_y;
        float m_z;
        uint32_t m_abgr;

        static void init()
        {
            ms_decl
                .begin()
                .add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
                .add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
                .end();
        };

        static bgfx::VertexDecl ms_decl;
    };

    bgfx::VertexDecl PosColorVertex::ms_decl;

    static PosColorVertex s_cubeVertices[] =
    {
        { -1.0f,  1.0f,  1.0f, 0xff000000 },
        { 1.0f,  1.0f,  1.0f, 0xff0000ff },
        { -1.0f, -1.0f,  1.0f, 0xff00ff00 },
        { 1.0f, -1.0f,  1.0f, 0xff00ffff },
        { -1.0f,  1.0f, -1.0f, 0xffff0000 },
        { 1.0f,  1.0f, -1.0f, 0xffff00ff },
        { -1.0f, -1.0f, -1.0f, 0xffffff00 },
        { 1.0f, -1.0f, -1.0f, 0xffffffff },
    };

    static const uint16_t s_cubeTriList[] =
    {
        0, 1, 2, // 0
        1, 3, 2,
        4, 6, 5, // 2
        5, 6, 7,
        0, 2, 4, // 4
        4, 2, 6,
        1, 5, 3, // 6
        5, 7, 3,
        0, 4, 1, // 8
        4, 5, 1,
        2, 3, 6, // 10
        6, 3, 7,
    };

    static const uint16_t s_cubeTriStrip[] =
    {
        0, 1, 2,
        3,
        7,
        1,
        5,
        0,
        4,
        2,
        6,
        7,
        4,
        5,
    };    

    SceneRenderer::SceneRenderer()
    {
        BX_UNUSED(s_cubeTriList, s_cubeTriStrip);

        // Create vertex stream declaration.
        PosColorVertex::init();

        // Create static vertex buffer.
        m_vbh = bgfx::createVertexBuffer(
            // Static data can be passed with bgfx::makeRef
            bgfx::makeRef(s_cubeVertices, sizeof(s_cubeVertices))
            , PosColorVertex::ms_decl
        );

        // Create static index buffer.
        m_ibh = bgfx::createIndexBuffer(
            // Static data can be passed with bgfx::makeRef
            bgfx::makeRef(s_cubeTriStrip, sizeof(s_cubeTriStrip))
        );


        bgfx::RendererType::Enum type = bgfx::getRendererType();

        bgfx::ShaderHandle vsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_cubes");
        bgfx::ShaderHandle fsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_cubes");

        // Create program from shaders.
        //m_program = loadProgram("vs_cubes", "fs_cubes");
        m_program = bgfx::createProgram(vsh, fsh, true /* destroy shaders when program is destroyed */);
		
		// DEBUG: Move somewhere else
		scene::ComponentStorage storage;
		storage.registerComponentType<Transform>();

		Transform* trans = storage.getComponent<Transform>(0);
		trans = storage.getComponent<Transform>(0);

    }

    SceneRenderer::~SceneRenderer()
    {
        // Cleanup.
        bgfx::destroyIndexBuffer(m_ibh);
        bgfx::destroyVertexBuffer(m_vbh);
        bgfx::destroyProgram(m_program);
    }

    void SceneRenderer::update(int width, int height, float time)
    {
        float at[3] = { 0.0f, 0.0f,   0.0f };
        float eye[3] = { 0.0f, 0.0f, -35.0f };

        // Set view and projection matrix for view 0.
        float view[16];
        bx::mtxLookAt(view, eye, at);

        float proj[16];
        bx::mtxProj(proj, 60.0f, float(width) / float(height), 0.1f, 100.0f, bgfx::getCaps()->homogeneousDepth);
        bgfx::setViewTransform(0, view, proj);
		
        // Set view 0 default viewport.
        bgfx::setViewRect(0, 0, 0, uint16_t(width), uint16_t(height));           

        // This dummy draw call is here to make sure that view 0 is cleared
        // if no other draw calls are submitted to view 0.
        bgfx::touch(0);

        // Submit 11x11 cubes.
        for (uint32_t yy = 0; yy < 11; ++yy)
        {
            for (uint32_t xx = 0; xx < 11; ++xx)
            {
                float mtx[16];
                bx::mtxRotateXY(mtx, time + xx*0.21f, time + yy*0.37f);
                mtx[12] = -15.0f + float(xx)*3.0f;
                mtx[13] = -15.0f + float(yy)*3.0f;
                mtx[14] = 0.0f;

                // Set model matrix for rendering.
                bgfx::setTransform(mtx);

                // Set vertex and index buffer.
                bgfx::setVertexBuffer(m_vbh);
                bgfx::setIndexBuffer(m_ibh);

                // Set render states.
                bgfx::setState(0
                    | BGFX_STATE_DEFAULT
                    | BGFX_STATE_PT_TRISTRIP
                );

                // Submit primitive for rendering to view 0.
                bgfx::submit(0, m_program);
            }
        }
    }
}
