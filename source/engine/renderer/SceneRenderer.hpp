#pragma once

#include "engine/bgfx_utils/common.h"
#include "engine/bgfx_utils/bgfx_utils.h"

namespace green
{
    class SceneRenderer
    {
    public:
                SceneRenderer();
                ~SceneRenderer();

        void    update(int width, int height, float time);

    private:

        bgfx::VertexBufferHandle    m_vbh;
        bgfx::IndexBufferHandle     m_ibh;
        bgfx::ProgramHandle         m_program;

    };
}