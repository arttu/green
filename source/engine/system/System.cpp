#include "engine/system/System.hpp"

namespace green
{
	namespace system
	{
		void init()
		{
			// Set custom assert handler
			pow2::Assert::SetHandler(handleAssert);
		}
	}
}

