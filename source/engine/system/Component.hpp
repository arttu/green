#pragma once

#include "engine/system/DataTypes.hpp"

namespace green
{
	namespace system
	{
		class Component
		{

		};
	}


	// Some actual components
	// TODO: Move somewhere else

	class Transform : public system::Component
	{
		Mat		translation;
		Qua		rotation; 
		Float3	scale;
	};
}
