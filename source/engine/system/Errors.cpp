#include "engine/system/Errors.hpp"

namespace green
{
	namespace system
	{
		pow2::Assert::FailBehavior handleAssert(const char * condition, const char * msg, const char * file, int line)
		{
			Log("Assertion failed (%s:%d): '%s' %s\n", FILENAME_FROM(file), line, condition, msg);
			return pow2::Assert::Halt;
		}
	}
}

