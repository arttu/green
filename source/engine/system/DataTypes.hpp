#pragma once

#include <mathgeolib/MathGeoLib.h>

namespace green
{
	using Id = uint32_t;

	using Qua = mgl::Quat;

	using Mat = mgl::float4x4;

	using Float2 = mgl::float2;
	using Float3 = mgl::float3;
	using Float4 = mgl::float4;
}