#pragma once

#include "engine/system/Component.hpp"
#include "engine/system/Errors.hpp"

#include <unordered_map>
#include <vector>
#include <typeinfo>
#include <type_traits>

namespace green
{
	namespace scene
	{
		class ComponentStorage
		{
		public:

			ComponentStorage();

			template <typename T>
			T* getComponent(Id id) 
			{
				ComponentTypeId typeId = typeid(T).hash_code();

				auto& table = m_storage[typeId];
				return table.find(id) != table.end() ? static_cast<T*>(&table[id]) : nullptr;
			}

			template <typename T>
			void registerComponentType()
			{
				ComponentTypeId typeId = typeid(T).hash_code();
				for (auto& registered : m_registeredTypes)
				{
					GR_ASSERT(typeId != registered, "Tried to register the same component type twice");
				}

				m_registeredTypes.emplace_back(typeId);

				m_storage[typeId] = std::unordered_map<Id, system::Component>();
			}

		private:
			using ComponentTypeId = uint64_t; 

			//std::unordered_map<uint32_t, system::Transform> m_transforms;
			std::unordered_map<ComponentTypeId, std::unordered_map<Id, system::Component>> m_storage;

			std::vector<ComponentTypeId> m_registeredTypes;
		};
	}
}