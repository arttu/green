﻿using System;
using System.Runtime.InteropServices;

using static Editor.Win32.Kernel32;

namespace Editor.Infrastructure
{
    class EngineJobObject : IDisposable
    {
        private IntPtr _handle;
        private bool _disposed = false;

        public EngineJobObject()
        {
            {
                _handle = CreateJobObject(IntPtr.Zero, null);

                var info = new JOBOBJECT_BASIC_LIMIT_INFORMATION
                {
                    // Causes all processes associated with the job to terminate when the last handle to the job is closed.                    
                    LimitFlags = 0x2000 // JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE
                };

                var extendedInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION
                {
                    BasicLimitInformation = info
                };

                int length = Marshal.SizeOf(typeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
                IntPtr extendedInfoPtr = Marshal.AllocHGlobal(length);
                Marshal.StructureToPtr(extendedInfo, extendedInfoPtr, false);

                if (!SetInformationJobObject(_handle, JobObjectInfoType.ExtendedLimitInformation, extendedInfoPtr, (uint)length))
                {
                    uint err = GetLastError();

                    throw new Exception(string.Format("Unable to set information.  Errors: {0}, {1}", Marshal.GetLastWin32Error(), err));
                }
            }
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            Close();
            _disposed = true;
            GC.SuppressFinalize(this);
        }

        public void Close()
        {
            CloseHandle(_handle);
            _handle = IntPtr.Zero;
        }

        public bool AddProcess(IntPtr processHandle)
        {
            return AssignProcessToJobObject(_handle, processHandle);
        }
    }
}
