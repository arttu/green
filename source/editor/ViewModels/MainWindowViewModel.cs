﻿using System;
using System.Windows;
using System.Diagnostics;
using System.Threading;
using System.ComponentModel;

namespace Editor.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool _loading = true;

        private string _loadingText = "";
        public string LoadingText
        {
            get { return _loadingText; }
            set
            {
                if (value != _loadingText)
                    _loadingText = value;
                NotifyPropertyChanged(nameof(LoadingText));
            }
        }

        protected void NotifyPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        
        /// <summary>
        /// Sets the loading label in the UI thread.
        /// </summary>
        /// <param name="label"></param>
        private void SetLoadingLabel(string label)
        {
            Trace.Assert(Application.Current.Dispatcher.Thread == Thread.CurrentThread);
            LoadingText = label;
        }

        public void LoadingFinished()
        {
            _loading = false;
        }

        /// <summary>
        /// Thread entry point to display the loading process to the user.
        /// </summary>
        /// <remarks>TEMPORARY CODE, could be change with an image animation.</remarks>
        public void DisplayEngineLoadingProgress()
        {
            int dots = 0;
            int dir = 1;
            while (_loading)
            {
                string label = String.Format("Loading{0}{1}{2}",
                  dots > 0 ? "." : "", dots > 1 ? "." : "", dots > 2 ? "." : "");

                Thread.Sleep(500);
                Application.Current.Dispatcher.Invoke(() => SetLoadingLabel(label));

                dots += dir;
                if (1 > dots || dots > 2)
                    dir *= -1;
            }

            Application.Current.Dispatcher.Invoke(() => SetLoadingLabel("Viewport Loaded"));
        }
    }
}
