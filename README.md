# README #

This is a prototype project that creates a simple C++ 3D engine that is hosted in a separate WPF application process. 

### How do I get set up? ###

Install GnuWin32 utilities and add them to Windows PATH variable:

http://gnuwin32.sourceforge.net/packages/make.htm

http://gnuwin32.sourceforge.net/packages/coreutils.htm

http://gnuwin32.sourceforge.net/packages/libiconv.htm

http://gnuwin32.sourceforge.net/packages/libintl.htm

The solution should work out of the box with Visual Studio 2017. On Visual Studio 2015, change platform toolset of the Engine project to v140.

Make sure configuration is Debug or Release and target platform is x64.
